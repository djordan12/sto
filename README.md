## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Node JS & Express
1) Create server.js file to be executed with nodejs (node server.js)
    a) Add 1-3 in server.js
2) Install express, npm install --save express
    a) Create app.js file, which will hold the express app.
    b) Add 1-3, use middleware concepts if necessary.
3) Add app.js to server.js file, set 'port'.
    a) Add 4-6, can test app.js file now to ensure it's connected.
4) Add success & error handling with server.
5) Add nodemon, automatically restarts server anytime you make changes (npm install --save-dev nodemon)
    a) This wasn't installed globally, you must add the script into package.json ("start:server": "nodemon server.js")
    b) execute npm run start:server

## MongoDB
1) Download mongoDB - using a cloud solution, reference quick start guide
    a) Should have created cluster, cluster should be initialized
2) Add new user with R/W to any database
    a) create username & password (DB2hZ3LpYU6tUvAZ);
3) Add IP Whitelist, add 'current ip address' This will change occassionaly, if you can't connect to DB, update this.
    Note: Once this is deployed, the IP address will be the IP address of the server you deployed
4) Add Mongoose, mongoose allows you access Mongodb much easier. Mongoose is great for structured data, using specific modals.
    a) npm install --save mongoose
5) Back in MongoDB, click connect ('Collect your applications') steps.

## Connect MongoShell (To View DB, if needed)
1) Download MongoShell, follow guide from MongoDB - connect link..
2) Enter shell, cd bin, from here launch ./mongo ...etc. enter PW.

## Bcrypt
1) npm install --save bcryptjs, this encrpyts passwords with a hash algorithm.

## JSON Web Token
1) npm install --save jsonwebtoken