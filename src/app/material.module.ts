import { NgModule } from '@angular/core';
import { 
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatMenuModule,
    MatCheckboxModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatListModule,
    MatSortModule,
    MatSelectModule
    } from '@angular/material';

@NgModule({
    imports: [
        MatInputModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatExpansionModule,
        MatMenuModule,
        MatCheckboxModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatListModule,
        MatSortModule,
        MatSelectModule
    ],
    exports: [
        MatInputModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatExpansionModule,
        MatMenuModule,
        MatCheckboxModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatListModule,
        MatSortModule,
        MatSelectModule
    ]
})

export class MaterialModule {}