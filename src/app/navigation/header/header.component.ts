import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  userIsAuthenticated = false;
  userEmail: string;
  private authListenerSubs: Subscription;

  constructor(private authService: AuthService) {

  }

  ngOnInit() {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.userEmail = this.authService.getUserEmail();
    // Subscribe to the subject, this will update this.authListenerSubs anytime the subject changes.
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
        this.userEmail = this.authService.getUserEmail();
      }
    );
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
  }

}
