const mongoose = require("mongoose");

// npm install --save mongoose-unique-validator
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
    // unique is not for validation
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true }
});

//Throws error if user is not unique.
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);

