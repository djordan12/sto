/**
 * #1 App.js using express framework built on nodejs
 */
const express = require("express");

/**
 * 
 */
const bodyParser = require("body-parser");

/**
 * To use mongoose's methods on MongoDB
 */
const mongoose = require("mongoose");

/**
 * Organizing routes
 */
const userRoutes = require("./routes/user");
const homeRoutes = require("./routes/home");

/**
 * #2 Creates an express application
 */
const app = express();

/**
 * Connect to mongoDB when your node server starts.
 */
mongoose.connect("mongodb+srv://darren:DB2hZ3LpYU6tUvAZ@sto-cluster-sivyu.mongodb.net/node-angular?retryWrites=true",
    { useNewUrlParser: true }    
)
    .then(() => {
        console.log('Connected to database!');
    })
    .catch(() => {
        console.log('Connection failed!');
    });

/**
 * ????
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false } ));

/**
 * Add headers for CORS errors
 */
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers", 
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
    next();
})

/**
 * This uses a middleware and executes it.
 * Using the 'next()' method will continue on it's journey after it's executed.
 */
app.use((req, res, next) => {
    next();
});

/**
 * send will also explicity end the response and writing stream.
 */
// app.use((req, res, next) => {
//     res.send('Second middleware, Hello from express!');
// })

app.use("/api/user", userRoutes);
app.use("/api/home", homeRoutes);

/**
 * #3 This will export all the middleware.
 */
module.exports = app;
