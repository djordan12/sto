const jwt = require("jsonwebtoken");

// This is a what typical middleware looks like, it's just a function!
module.exports = (req, res, next) => {
    try {
        // Multiple ways to get token, use this one below.
        // Header comes in this format 'Bearer adsflkjadsakjf' <- token
        const token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, "secret_this_should_be_longer");
        next();
    } catch (error) {
        res.status(401).json({ message: 'Auth failed in check-auth!' });
    }

};