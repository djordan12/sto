const app = require("./backend/app");
const debug = require("debug")("node-angular");
const http = require("http");

const normalizePort = val => {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
};

const onError = error => {
    if (error.syscall !== "listen") {
        throw error;
    }
    const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
};

const onListening = () => {
    const addr = server.address();
    const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
    debug("Listening on " + bind);
};

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

const server = http.createServer(app);
server.on("error", onError);
server.on("listening", onListening);
server.listen(port);

// /** Originial steps before error handling
//  * #1 Http is a default package that comes with nodejs
//  */
// const http = require('http');

// /**
//  * #4 Add relative path to app.js file, app.js is the listener
//  */
// const app = require('./backend/app');

// const port = process.env.PORT || 3000;

// /**
//  * #5 Set reserved 'port' key to the same port as the listener.
//  */
// app.set('port', port);

// /**
//  * #2 Create server that returns requests and responses
//  * #6 Create 'app' server
//  */
// const server = http.createServer(app);

// /**
//  * #3 Use default port by hosting provider || PORT# during development will be 3000
//  */
// server.listen(port);